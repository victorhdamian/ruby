require 'benchmark'

# 0. array
a = Array.new(6)
i = 0
while i < a.length
  puts a[i] = i
  i += 1
end
puts a

# 1. uniq function
# define input set
data = [1,5,6,8,4,3,5,7,3,2,1]

# define function that takes an array and returns only the set of unique elements
def  showuniquevalueswfunction(array)
  # result array
  result = Array.new
  # check if input array is empty
  if array.length > 0 then
      # make the result array iqual to the input array uniq function which returns a new array by removing duplicate values in self.
      result = array.uniq
  else
      print "Input array is empty. It can not be empty"
  end
  return result
end

def showuniquevalueseteration(array)
  for i in array
    for j in array
      if array[i] == array[j] and i != j
        array.slice!(j)
      end
    end
  end
  return array
end

# 2. hex.to_s(2) hex to binary function & gsub function with hash as second argument
input= ["FF","E7","C3","E7","FF"]

# define function that takes in a hexahecimal number as a string,
# converts it to a binary number
# and outputs the binary number replacing 1 for * and 0 for space
def  converter( array)
    #define hash to convert binary to pixel
    #pixels = { "1" => "*", "0" => " " }
    output = Array.new
    #iterate to input array
    for i in array
      #populate output array with converted hex to binary values
      #use string hex function to convert input array of hex characters to binary
      output.push(array.to_s[i].hex.to_s(2))
    end
    #iterate to output array
    for i in output
      # use puts to ouput each element of the output array on a new line
      #use gsub function to replaces binary representation of hex input to pixel hash representation
      puts output.to_s[i].gsub(/[01]/, '1' => '*', '0' => ' ')
    end
end


# 3. toeplitz matrix array
# Inputs: nxn matrix array
# Output Output: true if matrix is Toeplitz, false - otherwise.
# A Toeplitz matrix is defined as a matrix which has every
# top/left - bottom/right diagonal made up of the same number.

# [1 2 3 4 5]
# [9 1 2 3 4]
# [7 9 1 2 3]
# [6 7 9 1 2]
# [3 6 7 9 1]

##

a = [[9, 1, 2, 3, 4],
     [7, 9, 1, 2, 3],
     [6, 7, 9, 1, 2],
     [3, 6, 7, 9, 1]]

def eval_toeplitz_matrix(matrix)
    m = matrix.size - 1
    n = matrix[0].size - 1
    m.times{ |i| n.times{ |j| return false if matrix[i][j] != matrix[i+1][j+1] } }
    return true
end

def is_toeplitz_matrix?(matrix)
    m = matrix.size - 1
    n = matrix[0].size - 1
    m.times do |i|
      n.times do |j|
        return false if matrix[i][j] != matrix[i+1][j+1]
      end
    end
    return true
end

# 4. count array values split by comas
=begin
Count the tream tags for each tag in the stream (array)
output the count per tag
Example:
stream 'host:m5d.xlarge, role:rabbit, region:us-east-2,'
tag host:m5d.xlarge

input
stream = [
  'host:m5d.xlarge, role:rabbit, region:us-east-2,',
  'host:m5.large, role:alastic, region:eu-west-1,',
  'host:m5.12xlarge, role:webserver, region:us-west-2,',
  'host:m5d.xlarge, role:mongo, region:us-east-1,',
  'host:m5.12xlarge, role:rabbit, region:us-east-1,',
  'host:m5.12xlarge, role:rabbit, region:us-west-2,'
]

Output:
host:m5d.xlarge: 2
 role:rabbit: 3
 region:us-east-2: 1
host:m5.large: 1
 role:alastic: 1
 region:eu-west-1: 1
host:m5.12xlarge: 3
 role:webserver: 1
 region:us-west-2: 2
 role:mongo: 1
 region:us-east-1: 2
=end

stream = [
  'host:m5d.xlarge, role:rabbit, region:us-east-2,',
  'host:m5.large, role:alastic, region:eu-west-1,',
  'host:m5.12xlarge, role:webserver, region:us-west-2,',
  'host:m5d.xlarge, role:mongo, region:us-east-1,',
  'host:m5.12xlarge, role:rabbit, region:us-east-1,',
  'host:m5.12xlarge, role:rabbit, region:us-west-2,'
]

tag_out = {}
count = 0
stream.length

stream.each do |stream_line|
  stream_line.split(',').each do |tag|
     if tag_out.has_key?(tag)
       count = tag_out[tag] + 1
     else
       count = 1
     end
     tag_out.store(tag,count)
  end
end

#5. loop vs inject
a = [5,6,7,8,9,10,34]
b = 10.times.map{ 20 + Random.rand(11) }

def ArraySum(arr)
 p arr
 size = arr.length
 total = 0
 index = 0
 puts "Sum by while loop"
 puts Benchmark.measure {
 while index < size
   total = total + arr[index]
   index += 1
 end
}
 puts "Sum with inject :+"
 puts Benchmark.measure {arr.inject(:+)}
 return total
end


#usage
# 1. uniq function
get_uniq_wfunction = showuniquevalueswfunction(data)
get_uniq_iteration =  showuniquevalueseteration(data)
print get_uniq_wfunction, " ",  get_uniq_iteration, "\n"
# 2. hex.to_s(2) hex to binary function & gsub function with hash as second argument
converter(input)
# 3. toeplitz matrix array
puts is_toeplitz_matrix?(a)
puts eval_toeplitz_matrix(a)
# 4. count array values split by comas
tag_out.each do |key, value|
  puts "#{key}: #{value}" "\n"
end
# 5. loop vs inject
puts ArraySum(a)
puts ArraySum(b)
